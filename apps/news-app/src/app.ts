import Fastify from 'fastify'
import fastifyCors from "@fastify/cors";
import fastifyPostgres from '@fastify/postgres'; '@fastify/postgres';
import type { FastifyInstance, FastifyServerOptions } from 'fastify'

import { v1 } from './app/routes/v1/v1'
import { corsConfig } from './app/config/cors'
import { dbConfig } from './app/config/db';

export function build(opts?: FastifyServerOptions): FastifyInstance {
  const fastify = Fastify({
    logger: {
      prettyPrint: process.env.NODE_ENV === 'development'
        ? {
          translateTime: 'HH:MM:ss Z',
          ignore: 'pid,hostname'
        }
        : false
    },
    ...opts
  })
  // Database connection
  fastify.register(fastifyPostgres, dbConfig)
  // CORS setup
  fastify.register(fastifyCors, corsConfig())
  // Register routes v1
  fastify.register(v1, { prefix: '/api/v1' })

  return fastify
}
