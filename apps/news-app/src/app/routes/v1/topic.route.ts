import type { FastifyInstance, FastifyRegisterOptions } from "fastify";
import TopicsController from "../../controllers/topics.controller";
import { TopicObjectInput, TopicObjectPartial } from "../../schemas/topic.schema";
import { EntitiesName, Schema } from "../../schemas/schema";

export function topicRoute(fastify: FastifyInstance,
  opts: FastifyRegisterOptions<{}>,
  done: (err?: Error) => void) {
  const controller = new TopicsController(fastify.pg, EntitiesName.TOPICS);
  fastify.get('/', Schema(), controller.findAllWithQuery);
  fastify.get('/:id', Schema(), controller.findOne);
  fastify.post('/', Schema({ body: TopicObjectInput }), controller.create);
  fastify.put('/:id', Schema({ body: TopicObjectPartial }), controller.update);
  fastify.delete('/:id', Schema(), controller.delete);
  done();
}
