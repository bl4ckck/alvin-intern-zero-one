import type { FastifyInstance, FastifyRegisterOptions } from "fastify";
import { EntitiesName, Schema } from "../../schemas/schema";
import NewsTopicsController from "../../controllers/newsTopics.controller";
import { NewsTopicObject, NewsTopicObjectPartial } from "../../schemas/newsTopic.schema";

export function newsTopicRoute(fastify: FastifyInstance,
  opts: FastifyRegisterOptions<{}>,
  done: (err?: Error) => void) {
  const controller = new NewsTopicsController(fastify.pg, EntitiesName.NEWS_TOPIC);
  fastify.get('/', Schema(), controller.findAllWithQuery);
  fastify.get('/:id', Schema(), controller.findOne);
  fastify.post('/', Schema({ body: NewsTopicObject }), controller.create);
  fastify.put('/:id', Schema({ body: NewsTopicObjectPartial }), controller.update);
  fastify.delete('/:id', Schema(), controller.delete);
  done();
}
