import type { FastifyInstance, FastifyRegisterOptions } from "fastify";
import NewsController from "../../controllers/news.controller";
import { NewsObjectInput, NewsObjectPartial } from "../../schemas/news.schema";
import { EntitiesName, Schema } from "../../schemas/schema";

export function newsRoute(fastify: FastifyInstance,
  opts: FastifyRegisterOptions<{}>,
  done: (err?: Error) => void) {
  const controller = new NewsController(fastify.pg, EntitiesName.NEWS);
  fastify.get('/', controller.findAllWithQuery);
  fastify.get('/:id', Schema(), controller.findOne);
  fastify.post('/', Schema({ body: NewsObjectInput }), controller.create);
  fastify.put('/:id', Schema({ body: NewsObjectPartial }), controller.update);
  fastify.delete('/:id', Schema(), controller.delete);
  done();
}
