import type { FastifyInstance, FastifyRegisterOptions } from "fastify";
import { newsRoute } from "./news.route";
import { newsTopicRoute } from "./newsTopic.route";
import { topicRoute } from "./topic.route";

export function v1(fastify: FastifyInstance,
  opts: FastifyRegisterOptions<{}>,
  done: (err?: Error) => void) {
  fastify.register(newsRoute, { prefix: '/news' },)
  fastify.register(topicRoute, { prefix: '/topics' })
  fastify.register(newsTopicRoute, { prefix: '/news-topics' })
  done();
}
