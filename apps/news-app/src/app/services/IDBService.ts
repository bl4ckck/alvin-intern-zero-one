import type { QueryResult } from "pg";
import type { EntitiesSchemaType } from "../schemas/schema";

export interface IDBService {
  findAll(customQuery?: string): Promise<QueryResult<EntitiesSchemaType>>;
  findAllWithQuery(queryString: EntitiesSchemaType,
    customQuery?: string): Promise<QueryResult<EntitiesSchemaType>>;
  findOne(id: string): Promise<QueryResult<EntitiesSchemaType>>;
  create(payload: EntitiesSchemaType): Promise<QueryResult<EntitiesSchemaType>>;
  update(id: string, payload: Partial<EntitiesSchemaType>): Promise<QueryResult<EntitiesSchemaType>>;
  delete(id: string): Promise<QueryResult<EntitiesSchemaType>>;
}
