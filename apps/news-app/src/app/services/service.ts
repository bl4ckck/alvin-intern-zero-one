import type { PostgresDb } from "@fastify/postgres";
import type { QueryResult } from "pg";
import type { EntitiesName, EntitiesSchemaType } from "../schemas/schema";
import type { IDBService } from "./IDBService";
import { format } from 'node-pg-format';

class Service<T extends EntitiesSchemaType> implements IDBService {
  private pg: PostgresDb;
  private pk: string;
  private entity: EntitiesName;
  private defaultColumn: string;
  private insertColumn: string;
  private inputSchema: T;
  private updateSchema: T;

  constructor(pg: PostgresDb, entity: EntitiesName, defaultColumn?: string,
    insertColumn?: string, inputSchema?: T, updateSchema?: T, pk?: string) {
    this.pg = pg;
    this.entity = entity;
    this.defaultColumn = defaultColumn;
    this.insertColumn = insertColumn;
    this.inputSchema = inputSchema;
    this.updateSchema = updateSchema;
    this.pk = pk ?? 'id';
  }

  get getPk(): string {
    return this.pk;
  }

  get getInputSchema(): T {
    return this.inputSchema;
  }

  get getUpdateSchema(): T {
    return this.updateSchema;
  }

  get getDefaultColumn(): string {
    return this.defaultColumn;
  }

  get getInsertColumn(): string {
    return this.insertColumn;
  }

  get getPg(): PostgresDb {
    return this.pg;
  }

  get getEntity(): string {
    return this.entity;
  }

  public findAll(customQuery?: string): Promise<QueryResult<T>> {
    return this.pg.query<T>(customQuery ?? `SELECT ${this.defaultColumn ?? '*'} FROM ${this.entity};`);
  }

  public findAllWithQuery(queryString: EntitiesSchemaType, customQuery?: string): Promise<QueryResult<T>> {
    if (Object.keys(queryString).length === 0) return this.findAll();

    let filter: string[] = [];

    for (let key in queryString) {
      filter.push(format('%I = %L', key, queryString[key]));
    }

    const result = format(`${customQuery ?? `SELECT ${this.defaultColumn ?? '*'} FROM ${this.entity}`}
                            WHERE %s`, filter.join(' AND '));
    return this.pg.query<T>(result);
  }

  public findOne(id: string): Promise<QueryResult<T>> {
    console.log(`SELECT ${this.defaultColumn ?? '*'}
                            FROM ${this.entity} WHERE ${this.pk} = $1;`)
    return this.pg.query<T>(`SELECT ${this.defaultColumn ?? '*'}
                            FROM ${this.entity} WHERE ${this.pk} = $1;`, [id]);
  }

  public create(payload: T): Promise<QueryResult<T>> {
    payload = {
      ...this.inputSchema,
      ...payload
    }
    const query = format(`INSERT INTO ${this.entity}(%s)
                        VALUES(%L) RETURNING ${this.defaultColumn};`,
                        this.insertColumn, Object.values(payload))
    return this.pg.query(query);
  }

  public update(id: string, payload: Partial<T>): Promise<QueryResult<T>> {
    let sets: string[] = [];

    for (let key in payload) {
      sets.push(format('%I = %L', key, payload[key]));
    }
    const query = format(`UPDATE ${this.entity} SET %s
                          WHERE ${this.pk} = %L RETURNING ${this.defaultColumn};`,
                          sets.join(','), id);
    return this.pg.query(query);
  }

  public delete(id: string): Promise<QueryResult<T>> {
    return this.pg.query<T>(`DELETE FROM ${this.entity}
                            WHERE ${this.pk} = $1 RETURNING ${this.defaultColumn};`, [id]);
  }
}

export default Service;
