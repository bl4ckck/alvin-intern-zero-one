import type { PostgresDb } from "@fastify/postgres";
import type { EntitiesName } from "../schemas/schema";
import Service from "./service";
import { NewsTopicObjectInputStatic, NewsTopicType } from "../schemas/newsTopic.schema";

export const primaryKeyNewsTopic = 'news_id';
export const defaultColumnNewsTopic = 'news_id, topic_id';
export const inputColumnNewsTopic = defaultColumnNewsTopic;
export class NewsTopicsService extends Service<NewsTopicType> {
  constructor(pg: PostgresDb, entity: EntitiesName.NEWS_TOPIC) {
    super(
      pg, entity,
      defaultColumnNewsTopic, inputColumnNewsTopic,
      NewsTopicObjectInputStatic, null, primaryKeyNewsTopic
    );
  }
}
