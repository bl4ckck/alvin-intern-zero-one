import type { PostgresDb } from "@fastify/postgres";
import { TopicObjectInputStatic, TopicType } from "../schemas/topic.schema";
import type { EntitiesName } from "../schemas/schema";
import Service from "./service";

export const defaultColumnTopic = 'id, name';
export const inputColumnTopic = 'name';
export class TopicsService extends Service<TopicType> {
  constructor(pg: PostgresDb, entity: EntitiesName.TOPICS) {
    super(pg, entity, defaultColumnTopic,
      inputColumnTopic, TopicObjectInputStatic);
  }
}
