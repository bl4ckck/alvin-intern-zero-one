import type { PostgresDb } from "@fastify/postgres";
import type { EntitiesName, EntitiesSchemaType } from "../schemas/schema";
import { QueryResult } from "pg";
import { NewsObjectInputStatic, NewsType } from "../schemas/news.schema";
import Service from "./service";

export const defaultColumnNews = 'id, title, content, status, date';
export const inputColumnNews = 'title, content, status';
export class NewsService extends Service<NewsType> {
  constructor(pg: PostgresDb, entity: EntitiesName.NEWS) {
    super(pg, entity, defaultColumnNews,
      inputColumnNews, NewsObjectInputStatic);
  }

  private selectQuery(): string {
    return `
      SELECT news.*, topics
      FROM news
      INNER JOIN (
        SELECT array_to_json(
          array_agg(
            json_build_object('id', T.id, 'name', T.name)
          )
        ) topics,
        NT.news_id
        FROM news_topics NT
          INNER JOIN topics T ON NT.topic_id = T.id
        GROUP BY NT.news_id
      ) topics
      ON topics.news_id = news.id
    `;
  }

  public findAll(customQuery?: string): Promise<QueryResult<NewsType>> {
    return super.findAll(this.selectQuery());
  }

  public findAllWithQuery(queryString: EntitiesSchemaType,
    customQuery?: string): Promise<QueryResult<NewsType>> {
    return super.findAllWithQuery(queryString, this.selectQuery());
  }
}
