import type { PostgresDb } from "@fastify/postgres";
import type { EntitiesName } from "../schemas/schema";
import { NewsService } from "../services/newsService";
import Controller from "./controller"

class NewsController extends Controller<NewsService> {
  constructor(pg: PostgresDb, entity: EntitiesName.NEWS) {
    super(new NewsService(pg, entity));
  }
}

export default NewsController;
