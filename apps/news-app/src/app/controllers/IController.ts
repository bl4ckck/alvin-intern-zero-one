import type { FastifyReply, FastifyRequest } from "fastify";
import type { EntitiesSchemaType } from "../schemas/schema";

export interface IController {
  findAll(req: FastifyRequest, reply: FastifyReply): void | Promise<void>;
  findAllWithQuery(req: FastifyRequest<{ Querystring: EntitiesSchemaType }>,
    reply: FastifyReply): void | Promise<void>;
  findOne(req: FastifyRequest, reply: FastifyReply): void | Promise<void>;
  create(req: FastifyRequest, reply: FastifyReply): void | Promise<void>;
  update(req: FastifyRequest, reply: FastifyReply): void | Promise<void>;
  delete(req: FastifyRequest, reply: FastifyReply): void | Promise<void>;
}
