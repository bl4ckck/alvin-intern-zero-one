import type { FastifyReply, FastifyRequest, RequestGenericInterface } from "fastify";
import type { IDBService } from "../services/IDBService";
import { EntitiesSchemaType, ResponseSchema } from "../schemas/schema";
import { IController } from "./IController";

export interface DefaultReqParam {
  id: string;
}
export interface IReqWithParam<T extends RequestGenericInterface>
  extends FastifyRequest<T & { Params: DefaultReqParam }> { }

class Controller<Service extends IDBService> implements IController {
  private service: Service;

  constructor(service: Service) {
    this.service = service;
  }

  get getService(): Service {
    return this.service;
  }

  findAll = async (req: FastifyRequest, reply: FastifyReply): Promise<void> => {
    try {
      const { rows } = await this.service.findAll();
      reply.send(ResponseSchema(rows));
    } catch (error) {
      reply.send(ResponseSchema({}, error.message, true));
    }
  }

  findAllWithQuery = async (req: FastifyRequest<{ Querystring: EntitiesSchemaType }>,
    reply: FastifyReply): Promise<void> => {
    try {
      const { rows } = await this.service.findAllWithQuery(req.query);
      reply.send(ResponseSchema(rows));
    } catch (error) {
      reply.send(ResponseSchema({}, error.message, true));
    }
  }

  findOne = async (req: IReqWithParam<{}>, reply: FastifyReply): Promise<void> => {
    const { id } = req.params;
    try {
      const { rows } = await this.service.findOne(id);
      reply.send(ResponseSchema(rows));
    } catch (error) {
      reply.send(ResponseSchema({}, error.message, true));
    }
  }

  create = async (req: FastifyRequest<{ Body: EntitiesSchemaType }>,
    reply: FastifyReply): Promise<void> => {
    try {
      const { rows } = await this.service.create(req.body);
      reply.send(ResponseSchema(rows, 'Create Success'));
    } catch (error) {
      reply.send(ResponseSchema({}, error.message, true));
    }
  }

  update = async (req: IReqWithParam<{ Body: Partial<EntitiesSchemaType> }>,
    reply: FastifyReply): Promise<void> => {
    const { id } = req.params;
    try {
      const { rows } = await this.service.update(id, req.body);
      reply.send(ResponseSchema(rows, 'Update Success'));
    } catch (error) {
      reply.send(ResponseSchema({}, error.message, true));
    }
  }

  delete = async (req: IReqWithParam<{}>, reply: FastifyReply): Promise<void> => {
    const { id } = req.params;
    try {
      const { rows } = await this.service.delete(id);
      reply.send(ResponseSchema(rows, `Delete with ID ${id} Success`));
    } catch (error) {
      reply.send(ResponseSchema({}, error.message, true));
    }
  }
}

export default Controller;
