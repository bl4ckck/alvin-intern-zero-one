import type { PostgresDb } from "@fastify/postgres";
import type { EntitiesName } from "../schemas/schema";
import { NewsTopicsService } from "../services/newsTopicsService";
import Controller from "./controller"

class NewsTopicsController extends Controller<NewsTopicsService> {
  constructor(pg: PostgresDb, entity: EntitiesName.NEWS_TOPIC) {
    super(new NewsTopicsService(pg, entity));
  }
}

export default NewsTopicsController;
