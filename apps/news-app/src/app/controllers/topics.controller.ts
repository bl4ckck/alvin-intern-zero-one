import type { PostgresDb } from "@fastify/postgres";
import type { EntitiesName } from "../schemas/schema";
import { TopicsService } from "../services/topicsService";
import Controller from "./controller"

class TopicsController extends Controller<TopicsService> {
  constructor(pg: PostgresDb, entity: EntitiesName.TOPICS) {
    super(new TopicsService(pg, entity));
  }
}

export default TopicsController;
