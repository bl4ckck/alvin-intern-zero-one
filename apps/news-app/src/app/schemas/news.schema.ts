import { Static, Type } from '@sinclair/typebox'

export const StatusNewsEnum = Type.Enum({
  draft: 'draft',
  deleted: 'deleted',
  published: 'published'
})

export const NewsObject = Type.Object({
  id: Type.String({ format: 'uuid' }),
  title: Type.String({ minLength: 3, maxLength: 255 }),
  content: Type.String({ minLength: 3 }),
  status: StatusNewsEnum,
  date: Type.Any()
});

export const NewsObjectInput = Type.Omit(NewsObject, ['id', 'date']);
export const NewsObjectInputStatic: Static<typeof NewsObjectInput> =
  NewsObjectInput.$static = {
    title: null,
    content: null,
    status: 'draft'
  };

export const NewsObjectPartial = Type.Partial(NewsObject);
export const NewsObjectPartialStatic: Static<typeof NewsObjectPartial> =
  NewsObjectPartial.$static = {
    id: null,
    title: null,
    content: null,
    status: 'draft',
    date: null
  };

export const NewsArrayOfObject = Type.Array(NewsObject);

export type NewsType = Static<typeof NewsObject | typeof NewsObjectPartial
  | typeof NewsObjectInput | typeof NewsArrayOfObject>;

export type StatusNewsType = Static<typeof StatusNewsEnum>;
