import { Static, Type } from '@sinclair/typebox'
import { NewsArrayOfObject, NewsObject } from './news.schema';
import { NewsTopicArrayOfObject, NewsTopicObject } from './newsTopic.schema';
import { TopicArrayOfObject, TopicObject } from './topic.schema';

export const ResponseObject = Type.Object({
  error: Type.Boolean(),
  message: Type.String(),
  data: Type.Union([
    NewsObject, NewsArrayOfObject,
    TopicObject, TopicArrayOfObject,
    NewsTopicObject, NewsTopicArrayOfObject,
    Type.Object({})
  ]),
});
export type ResponseType = Static<typeof ResponseObject>;
