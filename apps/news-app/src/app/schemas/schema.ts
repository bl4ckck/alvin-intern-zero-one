import type { FastifySchema } from "fastify";
import { ResponseObject, ResponseType } from "./response.schema";

export type EntitiesSchemaType = {};

export enum EntitiesName {
  NEWS = "news",
  TOPICS = "topics",
  NEWS_TOPIC = "news_topics",
}

export const ResponseSchema: (data: EntitiesSchemaType, message?: string,
  isError?: boolean) => ResponseType = (data, message = '', isError = false) => {
    return {
      error: isError,
      data,
      message
    }
  }

export const Schema: (opts?: FastifySchema) => { schema: FastifySchema } = (opts) => {
  return {
    schema: {
      response: {
        200: ResponseObject,
        201: ResponseObject,
        500: ResponseObject,
      },
      ...opts
    },
  }
}
