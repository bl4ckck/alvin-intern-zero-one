import { Static, Type } from '@sinclair/typebox'

export const NewsTopicObject = Type.Object({
  news_id: Type.String({ format: 'uuid' }),
  topic_id: Type.String({ format: 'uuid' })
});

export const NewsTopicObjectInputStatic: Static<typeof NewsTopicObject> =
  NewsTopicObject.$static = {
    news_id: null,
    topic_id: null
  };

export const NewsTopicObjectPartial = Type.Partial(NewsTopicObject);
export const NewsTopicObjectPartialStatic: Static<typeof NewsTopicObjectPartial> =
  NewsTopicObjectPartial.$static = {
    news_id: null,
    topic_id: null
  };

export const NewsTopicArrayOfObject = Type.Array(NewsTopicObject);

export type NewsTopicType = Static<typeof NewsTopicObject | typeof NewsTopicObjectPartial
  | typeof NewsTopicArrayOfObject>;
