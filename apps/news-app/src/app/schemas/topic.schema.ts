import { Static, Type } from '@sinclair/typebox'

export const TopicObject = Type.Object({
  id: Type.String({ format: 'uuid' }),
  name: Type.String({ minLength: 3, maxLength: 255 })
});

export const TopicObjectInput = Type.Omit(TopicObject, ['id']);
export const TopicObjectInputStatic: Static<typeof TopicObjectInput> =
  TopicObjectInput.$static = {
    name: null
  };

export const TopicObjectPartial = Type.Partial(TopicObject);
export const TopicObjectPartialStatic: Static<typeof TopicObjectPartial> =
  TopicObjectPartial.$static = {
    id: null,
    name: null
  };

export const TopicArrayOfObject = Type.Array(TopicObject);

export type TopicType = Static<typeof TopicObject | typeof TopicObjectPartial
  | typeof TopicObjectInput | typeof TopicArrayOfObject>;
