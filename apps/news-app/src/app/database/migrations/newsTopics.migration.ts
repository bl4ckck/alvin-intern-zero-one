export const newsTopicsMigration: string = `
  CREATE TABLE IF NOT EXISTS news_topics (
    news_id UUID NOT null REFERENCES news(id) ON UPDATE CASCADE ON DELETE cascade,
    topic_id UUID NOT null REFERENCES topics(id) ON UPDATE CASCADE ON DELETE cascade,
    PRIMARY KEY (news_id, topic_id)
  );
`
