export const newsMigration: string = `
  CREATE TABLE IF NOT EXISTS news (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    status news_status NOT NULL,
    date TIMESTAMP NOT NULL DEFAULT NOW()
  );
`
