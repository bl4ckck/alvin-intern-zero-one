import { runQuery } from "../runQuery";
import { uuidMigration as UUID } from "./uuid.migration";
import { enumMigration as ENUM } from "./enum.migration";
import { newsMigration as NEWS } from "./news.migration";
import { topicsMigration as TOPICS } from "./topics.migration";
import { newsTopicsMigration as NEWS_TOPICS } from "./newsTopics.migration";

runQuery([
  UUID,
  ENUM,
  NEWS,
  TOPICS,
  NEWS_TOPICS
]);
