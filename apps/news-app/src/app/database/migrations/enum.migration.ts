export const enumMigration: string = `
  BEGIN;

  DO $$
  BEGIN
      IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'news_status') THEN
          CREATE TYPE news_status AS
          ENUM ('draft', 'deleted', 'published');
      END IF;
  END
  $$;
  COMMIT;
`
