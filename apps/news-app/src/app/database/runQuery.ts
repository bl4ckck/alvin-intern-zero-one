import type { FastifyInstance, FastifyRegisterOptions } from "fastify";
import { build } from "../../app";

export function runQuery(data: Array<string>) {
  const server = build();
  server.register(plugin);

  async function plugin(fastify: FastifyInstance,
    opts: FastifyRegisterOptions<{}>,
    done: (err?: Error) => void) {
    try {
      for (let i = 0; i < data.length; i++) {
        await fastify.pg.query(data[i]);
        fastify.log.info(`Running query #${i + 1} "${data[i]}" success!`);
      }
    } catch (error) {
      fastify.log.error(error);
    }

    done();
  }

  server.close();
}
