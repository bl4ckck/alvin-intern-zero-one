import { faker } from '@faker-js/faker';

export const TOPICS_ID = [
  '7fa0490d-e347-4b03-847f-9a97f010f121',
  'e80a6aa7-2bd1-4c01-995e-161d01bd8529',
  'aee2c485-12b7-4f7e-acb4-371331a3347b',
  'ca6d1318-0aa4-45fe-9378-6bfa91d4c40e',
  '9dd49bae-652e-401d-883c-436d646b1fe7',
  'ef087453-3421-4003-9114-2ed3ffdb24f9',
  'a0bff763-0dc5-4be7-ab0d-09b52247ef2d',
  '4098da95-4365-4345-8916-b23270482186',
  '0a050437-0a87-4f5b-aad8-5113f19e7404',
  'cd459746-70a5-4285-9a54-b459c62ffe9a'
];

function generateTopics(): string {
  let query: string = '';

  for (let i = 0; i < TOPICS_ID.length; i++) {
    const isLast = i === TOPICS_ID.length - 1 ? ';' : ',\n';
    query +=
      `('${TOPICS_ID[i]}', '${faker.company.bsNoun()}')${isLast}`;
  }

  return query;
}

export const topicsSeeder: string = `
  DELETE FROM topics;
  INSERT INTO topics (id, name) VALUES ${generateTopics()}
`;
