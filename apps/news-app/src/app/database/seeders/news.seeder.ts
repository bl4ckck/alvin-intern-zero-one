import { faker } from '@faker-js/faker';

export const NEWS_ID = [
  '17e331b6-2e5e-4394-91a0-062f1cdec7ea',
  '5b98afff-5c15-4e15-8c42-bbd38d051097',
  '4a8cd190-f09c-4ff3-9e8d-5b56b2ea6ae1',
  '95a4f301-e783-4e9b-a447-8d4900766cf5',
  '4184ed8b-2c89-4c43-a90c-661b966fe5de'
];

function generateNews(): string {
  let query: string = '';

  for (let i = 0; i < NEWS_ID.length; i++) {
    const isLast = i === NEWS_ID.length - 1 ? ';' : ',\n';
    const STATUS_NEWS = faker.helpers.arrayElement(['draft', 'deleted', 'published']);

    query +=
      `('${NEWS_ID[i]}', '${faker.lorem.words(10)}', '${faker.lorem.text()}', '${STATUS_NEWS}')${isLast}`;
  }

  return query;
}

export const newsSeeder: string = `
  DELETE FROM news;
  INSERT INTO news (id, title, content, status) VALUES ${generateNews()}
`;
