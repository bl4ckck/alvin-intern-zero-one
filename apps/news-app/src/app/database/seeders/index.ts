import { runQuery } from "../runQuery";
import { newsSeeder as NEWS } from "./news.seeder";
import { topicsSeeder as TOPICS } from "./topics.seeder";
import { newsTopicsSeeder as NEWS_TOPICS } from "./newsTopics.seeder";

runQuery([
  NEWS,
  TOPICS,
  NEWS_TOPICS
]);
