import { faker } from '@faker-js/faker';
import { NEWS_ID } from './news.seeder';
import { TOPICS_ID } from './topics.seeder';

function generateNewsTopics(): string {
  let query: string = '', maxTopics = faker.datatype.number({ min: 1, max: 3 });

  for (let i = 0; i < NEWS_ID.length; i++) {
    if (i + 1 === NEWS_ID.length - 1) maxTopics = 1;
    const isLast = i === NEWS_ID.length - 1 ? ';' : ',\n';
    const RANDOM_TOPIC = faker.helpers.uniqueArray(TOPICS_ID, maxTopics);

    for (let j = 0; j < maxTopics; j++) {
      query +=
        `('${NEWS_ID[i]}', '${RANDOM_TOPIC[j]}')${isLast}`;
    }
  }

  return query;
}

export const newsTopicsSeeder: string = `
  DELETE FROM news_topics;
  INSERT INTO news_topics (news_id, topic_id) VALUES ${generateNewsTopics()}
`;
