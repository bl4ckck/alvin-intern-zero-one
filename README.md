

# Alvin Naufal: Internship Test Zero One

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Smart, Fast and Extensible Build System**

## Installation
1. Create `.env` based on `.env.example` in `apps/news-app/` directory
2. Install dependencies
```sh
yarn install
```
3. Run database migration & seeder
```sh
nx run news-app:db:migrate
nx run news-app:db:seed
```
4. Run app
```sh
yarn start news-app
```

## Endpoint API
You can access the API from [Insomnia collection](https://gitlab.com/bl4ckck/alvin-intern-zero-one/-/raw/main/apps/news-app/Insomnia_AlvinNaufal_NEWS_API.json?inline=false) or [HAR Format](https://gitlab.com/bl4ckck/alvin-intern-zero-one/-/raw/main/apps/news-app/AlvinNaufal_NEWS_API.har?inline=false)

